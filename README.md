# DDP_NA16B112

Online Hybrid Motion Planning for Unmaned Aerial Vehicles in Planar Environments

## File descriptions
*  imported/DDP Ashish Maknikar.ipynb - The jupyter notebook where most of the rough prototyping is done. 
*  hybrid_na16b112/* - The final code files generated after collating the code from DDP Ashish Maknikar.ipynb
*  hybrid_na16b112/sim.py - Run to run the simualtion
*  hybrid_na16b112/pn*.py - The files for the respective eponymous algorithms.
*  hybrid_na16b112/common.py - File containg some common functions used in all three.
*  hybrid_na16b112/Sim Data Processing.ipynb - to get the metrics a rough ipynb notebook. All the metrics will be generated in the meta.csv file except the trunc_sim_time(TODO:add trunc sim time to meta.csv)

## Cloning repository
` git clone --depth 1 https://gitlab.com/asmaknikar/ddp_na16b112.git `
remove `--depth 1` to get the complete history