import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits import mplot3d
from scipy.spatial.transform import Rotation
from tqdm.notebook import tqdm
from datetime import datetime
from pathlib import Path

from common import *
from pnpf import pnpf
from pncc import pncc
from pnvo import pnvo


def generate_workspace(seed,start,end,n_obs,v_r_mod=12.5,v_t_mod=7.5,v_obs_range=(5,10),r_rand_ang_lim=(0,45),t_rand_ang_lim=(0,45),R=0):
    """Generates workspace for simulation

    Parameters
    ----------
    seed : int
        The input seed for numpy random functions
    start : int
        (start,start,start) is the beginning of the workspace
    end : [type]
        (end,end,end) is the end of the workspace
    n_obs : [type]
        [description]
    v_r_mod : float, optional
        |v_r|, by default 12.5
    v_t_mod : float, optional
        |v_t|, by default 7.5
    v_obs_range : tuple, optional
        (min(|v_obs|),max(|v_obs|)), by default (5,10)
    r_rand_ang_lim : tuple, optional
        The angle limit for random rotation of v_rvector
    t_rand_ang_lim : tuple, optional
        The angle limit for random rotation of v_t vector, by default 45
    R : int, optional
        The radius of obstacle. We use it to generate obstacles at a distance of 2*(R,R,R) from p_r, by default 0

    Notes
    ------
    * We intialise the v_r and v_t vecs towards p_t and p_r respectively. We then rotate them about a random axis by a random angle(b/n -+v_rt_rand_an_lim).
        This is to ensure that the robot and targets pass throught he uniform obstacle cloud generated in the workspace
    """ 

    np.random.seed(seed)
    p_r = np.array([0,0,0])
    p_t = np.array([end,end,end])
    v_r = v_r_mod*Rotation.from_rotvec(
        np.random.uniform(*np.deg2rad(r_rand_ang_lim))*unit_vector(np.random.rand(3))).apply(unit_vector(p_t-p_r))
    v_t = v_t_mod*Rotation.from_rotvec(
        np.random.uniform(*np.deg2rad(t_rand_ang_lim))*unit_vector(np.random.rand(3))).apply(unit_vector(p_r-p_t))

    p_os,v_os = (np.array([np.random.uniform(start+1*R,end,n_obs),
                        np.random.uniform(start+1*R,end,n_obs),
                        np.random.uniform(start+1*R,end,n_obs)]).T,
                np.array([np.random.uniform(*v_obs_range)*v for v in np.apply_along_axis(unit_vector,1,np.random.uniform(-1,1,(n_obs,3)))]))
    
    return(p_r,v_r,p_t,v_t,p_os,v_os)

def save_csv(par,n_obs,seed,algo,his):
    out_file = Path(f'{par}/{n_obs}/{seed}/{algo}.csv')
    out_file.parent.mkdir(parents=True, exist_ok=True)
    his.to_csv(out_file)
    

if __name__=="__main__":
    timestamp = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
    par = f"{timestamp}_toc_ash_all_obs_avo_hit_pfkrep_300_dt_5"
    plot = False
    R = 4
    rho = 20
    num_sim = 100

    # n_obs = 50
    # seed = 0
    all_meta = pd.DataFrame()
    for n_obs in [50,40,30,20,10]:
        seeds = np.random.choice(range(100000,2*100000), 10000, replace=False)
        np.save(f"seeds_{par}_n_obs_{n_obs}.npy",seeds)
        count = 0

        # hdf = pd.HDFStore(f"SimData_{timestamp}_nobs_{n_obs}.h5")
        pbar = tqdm(total=num_sim+1)
        for seed in seeds:
            print("seed",seed)
            p_r,v_r,p_t,v_t,p_os,v_os = generate_workspace(seed,0,100,n_obs,t_rand_ang_lim=(0,10),R=R)

            print("\n\n\n\nPNCC \n")
            his_cc,meta_cc = pncc(p_r,v_r,p_t,v_t,p_os,v_os,N=3,K=4,R=R,rho=rho,max_obs=2,num_accels=10,plot=plot)
            meta_cc['algo']='cc';meta_cc["n_obs"] = n_obs;meta_cc["seed"] = seed
            if(meta_cc["obs_avo_steps"]<10):
                continue
            # hdf.put(str(seed)+'/cc', his_cc, data_columns=True)
            # hdf.get_storer(str(seed)+'/cc').attrs.metadata = meta_cc
            
            print("\n\n\n\nPNVO \n")
            his_vo,meta_vo = pnvo(p_r,v_r,p_t,v_t,p_os,v_os,N=3,K=3,R=R,rho=rho,check_later=True,plot=plot)
            meta_vo['algo']='vo';meta_vo["n_obs"] = n_obs;meta_vo["seed"] = seed
            if(meta_vo["obs_avo_steps"]<10):
                continue
            # hdf.put(str(seed)+'/vo', his_cc, data_columns=True)
            # hdf.get_storer(str(seed)+'/vo').attrs.metadata = meta_vo

            print("\n\n\n\nPNPF \n")
            his_pf,meta_pf = pnpf(p_r,v_r,p_t,v_t,p_os,v_os,N=3,k_rep=300,k_att=0,R=R,rho=rho,plot=plot)
            meta_pf['algo']='pf';meta_pf["n_obs"] = n_obs;meta_pf["seed"] = seed
            # hdf.put(str(seed)+'/pf', his_pf, data_columns=True)
            # hdf.get_storer(str(seed)+'/pf').attrs.metadata = meta_pf
            if(meta_pf["obs_avo_steps"]<10):
                continue

            if(meta_pf["obs_avo_steps"]>9 and meta_cc["obs_avo_steps"]>9 and meta_vo["obs_avo_steps"]>9):
                save_csv(par,n_obs,seed,'pf',his_pf)
                save_csv(par,n_obs,seed,'cc',his_cc)
                save_csv(par,n_obs,seed,'vo',his_vo)

                all_meta = all_meta.append(meta_pf, ignore_index=True)
                all_meta = all_meta.append(meta_cc, ignore_index=True)
                all_meta = all_meta.append(meta_vo, ignore_index=True)

                count+=1
                pbar.update(1)
            
            if(count>num_sim):
                break

            print("\n\n\n\n\n\n")
        pbar.close()    
        print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n")

        # hdf.close()
        all_meta.to_csv(f'{par}/meta.csv')
    all_meta.to_csv(f'{par}/meta_fin.csv')
    

    
    # plot_gif(his)

