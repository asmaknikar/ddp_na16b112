import time

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import numpy as np
import pandas as pd
from mpl_toolkits import mplot3d
from scipy.spatial.transform import Rotation
from tqdm.notebook import tqdm

from common import (distance, get_azimuth_elevation,
                    get_uvec_from_azimuth_elevation, time_of_closest_approach,
                    unit_vector,plot_arrow,time_to_collision_ashish,time_to_collision_LPM,generate_workspace,save_csv)


def perpendicular_to(v):
    v_homogenous = np.array([v[0],v[1],1])
    direction = np.cross(np.array([0,0,1]),v_homogenous)
    return np.array([direction[0],direction[1]])

def time_to_col(p1,p2,v1,v2,rad):#CHECK DOT PRODUCT SHUD BE v1-v2.p2-p1
    toc = abs((distance(p1,p2) - 2*rad)/np.dot(v1-v2,unit_vector(p1-p2)))
    return toc


def get_r_th_phi(p,v):
#     rot_mat = Rotation.from_euler('ZYX', 
#         [ np.degrees(np.arctan2(p[1],p[0])),
#          np.degrees(np.arctan2(p[2],np.linalg.norm([p[0],p[1]]))),
#          0], degrees=True)
#     print(rot_mat.as_dcm())
#     print("azimuth",np.degrees(np.arctan2(p[1],p[0])))
#     print("elevation",np.degrees(np.arctan2(p[2],np.linalg.norm([p[0],p[1]]))))
    r1 = Rotation.from_rotvec([ 0,0,np.arctan2(p[1],p[0])])
    r2 = Rotation.from_rotvec([ 0,np.arctan2(p[2],np.linalg.norm([p[0],p[1]])),0])
    return(r2.apply(r1.inv().apply(v)))
#     return(rot_mat.inv().apply(v))

# def DG_a_cc(a_gam):
#     temp1 = (-0.5*K*(np.linalg.norm(v_rel)**2)/p_rel_mod**2)*(p_rel_mod**2*(v_th**2+v_phi**2))
#     temp2 = v_R*v_R*(-v_th*np.cos(a_gam)*np.sin(a_del-th)+v_phi*(np.cos(a_gam)*np.sin(phi)*np.cos(a_del-th)-np.sin(a_gam)*np.cos(phi)))
#     temp3 = v_R*(v_th**2+v_phi**2)*(np.cos(a_gam)*np.cos(phi)*np.cos(a_del-th)+(np.sin(a_gam)*np.sin(phi)))
#     a_mod = temp1/(temp2+temp3)

def get_r_th_phi2(p_r,p_t,v_r,v_t):
    b,a = get_azimuth_elevation(v_r)
    u,e = get_azimuth_elevation(v_t)
    th,ph = get_azimuth_elevation(p_t-p_r)
#     print(np.degrees([b,a,u,e,th,ph]))
    v_t_mod = np.linalg.norm(v_t)
    v_r_mod = np.linalg.norm(v_r)
#     print(v_t_mod,v_r_mod)
    v_th = (v_t_mod*np.cos(e)*np.sin(u-th))-(v_r_mod*np.cos(a)*np.sin(b-th))
    v_ph = v_t_mod*(-np.cos(e)*np.sin(ph)*np.cos((u-th))+ np.sin(e)*np.cos(ph))-v_r_mod*(-np.cos(a)*np.sin(ph)*np.cos((b-th))+np.sin(a)*np.cos(ph))
    v_R = v_t_mod*(np.cos(e)*np.cos(ph)*np.cos((u-th))+ np.sin(e)*np.sin(ph))-v_r_mod*(np.cos(a)*np.cos(ph)*np.cos((b-th))+ np.sin(a)*np.sin(ph))
    return(v_R,v_th,v_ph)



def get_vr2xaxis_rot(v_r):
    v_r_new = [np.linalg.norm(v_r),0,0]
    rot_vr = Rotation.from_rotvec(np.arccos(np.dot(unit_vector(v_r_new),unit_vector(v_r)))*unit_vector(np.cross(v_r,v_r_new)))
    return(rot_vr)

def get_perp_vecs(v,num=10):
    v = unit_vector(v)
    rot = get_vr2xaxis_rot(v)
    ths = np.linspace(0,2*np.pi,num)
    
    vecs = np.array([get_uvec_from_azimuth_elevation(np.pi/2,ths_i) for ths_i in ths])
    vecs_true = np.apply_along_axis(rot.inv().apply,1,vecs)
    
    return(vecs_true)

def get_perp_azs_els(v,num=10):
    vecs_true = get_perp_vecs(v,num)
    return(np.apply_along_axis(get_azimuth_elevation,1,vecs_true).T)

def cc_y(p_r,p_t,v_r,v_t,R):
    v_rel = v_t - v_r
    p_rel = p_t-p_r
    p_rel_mod = np.linalg.norm(p_rel)
    v_R,v_th,v_ph = get_r_th_phi2(p_r,p_t,v_r,v_t)
    tmp = v_th**2+v_ph**2
    return((((p_rel_mod**2)*tmp)/(tmp+v_R**2))-R**2)

def a_cc(p_r,v_r,p_o,v_o,K,R,a_del,a_gam,toc=None,eps=2):
    if(cc_y(p_r,p_o,v_r,v_o,R)<0):
        v_rel = v_o - v_r
        p_rel = p_o-p_r
        p_rel_mod = np.linalg.norm(p_rel)

        v_R,v_th,v_phi = get_r_th_phi2(p_r,p_o,v_r,v_o)
        # a_dir = np.cross(v_r,np.cross(p_rel,v_rel))
        a_dir = get_uvec_from_azimuth_elevation(a_del,a_gam)
        th,phi = get_azimuth_elevation(p_rel)

        #Getting the min K
        if(toc is None):
            # toc = time_of_closest_approach(p_r,p_o,v_r,v_o)
            toc = time_to_collision_ashish(p_r,p_o,v_r,v_o,R)
        K = max((1/toc)*np.log(min(-cc_y(p_r,p_o,v_r,v_o,R)/eps,2)),K)
        # K = max((1/toc)*np.log(4),K)
        
        temp1 = (-0.5*K*(np.linalg.norm(v_rel)**2)/p_rel_mod**2)*((p_rel_mod**2*(v_th**2+v_phi**2))-(R*R*(np.linalg.norm(v_rel)**2)))
        temp2 = v_R*v_R*(-v_th*np.cos(a_gam)*np.sin(a_del-th)+v_phi*(np.cos(a_gam)*np.sin(phi)*np.cos(a_del-th)-np.sin(a_gam)*np.cos(phi)))
        temp3 = v_R*(v_th**2+v_phi**2)*(np.cos(a_gam)*np.cos(phi)*np.cos(a_del-th)+(np.sin(a_gam)*np.sin(phi)))
        a_mod = temp1/(temp2+temp3)
        a_cc = a_mod*a_dir
        return(a_cc)
    else:
        return(np.array([0,0,0]))

def a_ppn(p_robot,v_robot,p_target,v_target,N,bounds):
    r = p_target - p_robot
    v_relative = v_target - v_robot
    # omega = np.cross([r[0],r[1],0],[v_relative[0],v_relative[1],0])/np.dot(r,r)
    omega = np.cross(r,v_relative)/np.dot(r,r)
    vr_cap = unit_vector(v_robot)
    # a_ppn = -N*np.cross([v_robot[0],v_robot[1],0],omega)
    a_ppn = -N*np.cross(v_robot,omega)
    # a_ppn = np.array([a_ppn[0],a_ppn[1]])
    if bounds==0:
        return a_ppn
    if bounds==1:
        return min(2,np.linalg.norm(a_ppn))*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)<2:
        a_ppn = 2*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)>3:
        a_ppn = 3*unit_vector(a_ppn)
    return a_ppn

def pncc(p_r,v_r,p_t,v_t,p_os,v_os,N=3,K=4,R=9,rho=None,max_obs=2,num_accels=10,plot=False):
    """Simulates the PN-CC algorithm(Proportional Navigation-Collision Cone)

    Parameters
    ----------
    p_r : ndarray(ndim=3)
        initial UAV position
    v_r : ndarray(ndim=3)
        initial UAV velocity
    p_t : ndarray(ndim=3)
        initial target position
    v_t : ndarray(ndim=3)
        initial target velocity
    p_os : ndarray(ndim=(n_obs,3))
        initial obstacles position
    v_os : ndarray(ndim=(n_obs,3))
        initial obstacles velocities
    N : int, optional
        PN constant, by default 3
    K : int, optional
        CC constant, by default 4
    R : int, optional
        Radius of obstacles, by default 9
    rho : [type], optional
        Maximum sensing distance of obstacles from IAV , by default None
    max_obs : int, optional
        Max number of obsacles that you wish for the CC avoidance to handle. , by default 2
    num_accels : int, optional
        Number of vectors perpendicular to the v_r to calculate CC accel. The best among those is chose by wehiging magnitude and perp from a_ppn, by default 10
    plot : bool, optional
        Flag to generate plot, by default False

    Returns
    -----------
    his : Pandas DataFram
        Simulation UAV,Target and obs data (pos,vel,accel) and simulation data

    Notes
    ------------
    Ashish
    """
    if(plot):
        fig = plt.figure()
        ax=fig.add_subplot(111,projection='3d')
        plot_arrow(ax,p_r,v_r,"o","blue")
        plot_arrow(ax,p_t,v_t,"o","green")
        plot_arrow(ax,p_os,v_os,"D","red")
        plt.show()

    
    his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_cc","a_ppn","accel","closest_dis","obs","comp_time","collision"])
    dt = 0.05

    omega_cc_bound = np.deg2rad(30)
    accel_cc_bound = np.linalg.norm(v_r)*omega_cc_bound
    print("accel_cc_bound",accel_cc_bound)
    if(rho is None):
        rho = 1*np.sqrt((2*((12.5+10)/omega_cc_bound)+R)*R)

    R = R+1 #margin of safety
    max_obs = 2
    time_cc = 0
    max_obs = 1

    sim_time_st = time.time()
    while distance(p_r,p_t)>2:
        collision = False
        comp_time_st = time.time()
        dist_os = np.linalg.norm(p_os-p_r,axis=1)
        
        accel_cc = np.array([0,0,0])
        accel_ppn = a_ppn(p_r,v_r,p_t,v_t,N,0)
        
        if(np.any(dist_os<R-1)):
            print("collision")
            collision = True
        
        ind_obs_coll = np.nonzero([(distance(p_oi,p_r)<rho and
                                    cc_y(p_r,p_oi,v_r,v_oi,R)<0 ) for (p_oi,v_oi) in zip(p_os,v_os)])[0]
        if(len(ind_obs_coll)>0):
            target_time_closest = time_of_closest_approach(p_r,p_t,v_r,v_t)
            p_os_coll = p_os[ind_obs_coll];v_os_coll = v_os[ind_obs_coll]
            # tocs = np.array([time_of_closest_approach(p_r,p_oi,v_r,v_oi) for (p_oi,v_oi) in zip(p_os_coll,v_os_coll)])
            tocs = np.array([time_to_collision_ashish(p_r,p_oi,v_r,v_oi,R) for (p_oi,v_oi) in zip(p_os_coll,v_os_coll)])
            
            #Ignoring obstacles whose toc<toc of target
            if((tocs<target_time_closest).any()):
                ind_obs_coll = ind_obs_coll[tocs<target_time_closest]
                tocs = tocs[tocs<target_time_closest]
                p_os_coll = p_os[ind_obs_coll];v_os_coll = v_os[ind_obs_coll]

            # # #perpendicular to a_ppn
            # a_dir = unit_vector(np.cross(v_r,accel_ppn))
            # a_del,a_gam = get_azimuth_elevation(a_dir)
            # accel_ccs = np.array([a_cc(p_r, v_r, p_oi, v_oi, K, R, a_del, a_gam) for p_oi,v_oi in zip(p_os_coll,v_os_coll)])

            a_dels,a_gams = get_perp_azs_els(v_r,num=num_accels)    #multiple a_ccs perpendicular to v_r
            accel_ccs = []
            for p_oi,v_oi,toc_i in zip(p_os_coll,v_os_coll,tocs):
                accel_cc_is = np.array([a_cc(p_r, v_r, p_oi, v_oi, K, R, a_del, a_gam,toc_i) 
                                        for a_del,a_gam in zip(a_dels,a_gams)])
                #weightage between perpendicularity to a_ppn and magnitude of a_cc
                accel_ccs.append(accel_cc_is[np.argmin(0.7*unit_vector(np.linalg.norm(accel_cc_is,axis=1))
                                                    +0.3*unit_vector(np.abs(np.dot(accel_cc_is,accel_ppn))))])
                                                    
            #Taking only max_obs accelerations with the lowest time of closes approach
            accel_ccs = np.array(accel_ccs)[np.argsort(tocs)[:max_obs]]
            tocs = tocs[np.argsort(tocs)[:max_obs]]
            
            #try adding sigmoid functionality to the weighted sum
            # accel_cc = np.sum((1-unit_vector(tocs))*accel_ccs,0)#1-unit
            accel_cc = np.sum(np.expand_dims(unit_vector(10/tocs),-1)*accel_ccs,0)#reciprocal

            accel_cc = accel_cc+np.dot(accel_ppn,unit_vector(accel_cc))*(-unit_vector(accel_cc))    #neutralising ppn from CC accn. NO NEED WHEN a_cc PERP TO a_ppn
            # accel_cc = min(np.linalg.norm(accel_cc),accel_cc_bound)*unit_vector(accel_cc)
        
        accel = accel_cc+accel_ppn
        accel = min(np.linalg.norm(accel),accel_cc_bound)*unit_vector(accel)    #bounding the accel
        
        comp_time_end = time.time()
        print(distance(p_r,p_t),round(np.linalg.norm(accel),2),round(np.linalg.norm(accel_cc),2),min(dist_os),ind_obs_coll)
        if(distance(p_r,p_t)>300):
            his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_cc","a_ppn","accel","closest_dis","obs","comp_time","collision"])
            break

        
        v_r = v_r + (accel*dt)
        p_r = p_r + v_r*dt
        time_cc = time_cc+dt
        p_os = p_os+(v_os*dt)
        accel_cc_prev = np.copy(accel_cc)
        S_coll_prev = set(ind_obs_coll)
        p_t = p_t + (v_t*dt)
        his = his.append(pd.Series([p_r,v_r,p_t,v_t,p_os,v_os,
                                    accel_cc,accel_ppn,accel,
                                np.min(dist_os),ind_obs_coll,comp_time_end-comp_time_st,collision], index = his.columns),ignore_index=True)
    metadata = dict(R=R-1,rho=rho,
                    tot_sim_comp_time = time.time()-sim_time_st,
                    trunc_sim_comp_time = np.sum(his.comp_time.values),
                    obs_avo_steps=len(np.nonzero(np.array(list(map(len,his.obs.values)))>0)[0]),
                    obs_collisions = len(np.nonzero(his.collision.values)[0]),
                    a_net_rms = np.sqrt(np.mean(np.apply_along_axis(lambda x:np.dot(x,x),1,np.stack(his.accel.values)))) if (len(his)) else 0,
                    len = len(his))
    return(his,metadata)



if __name__=="__main__":
    from collections import Counter
    from datetime import datetime
    # seeds = np.load("./seeds_R_4_rho_20_only_coll_for_pf.npy")
    seeds = np.load("./seeds_R_4_rho_20_for_pf.npy")
    # print(seeds)
    timestamp = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
    par = f"{timestamp}_toc_ash_pncc_R_wo_safety"
    

    plot = False
    R = 4
    rho = 20
    counter_list = []
    all_meta = pd.DataFrame()

    for seed,n_obs in seeds:
        p_r,v_r,p_t,v_t,p_os,v_os = generate_workspace(seed,0,100,n_obs,t_rand_ang_lim=(0,10),R=R)

        print("\n\n\n\nPNPF \n")
        print(seed,n_obs)

        his_cc,meta_cc = pncc(p_r,v_r,p_t,v_t,p_os,v_os,N=3,K=4,R=R,rho=rho,max_obs=2,num_accels=10,plot=plot)
        meta_cc['algo']='cc';meta_cc["n_obs"] = n_obs;meta_cc["seed"] = seed

        print(meta_cc["obs_collisions"])
        if(meta_cc["obs_collisions"]>0):
            counter_list.append(n_obs)
        print("COUNTER",Counter(counter_list))
        if(meta_cc["obs_avo_steps"]<10):
            print("SHIZNIT")

        save_csv(par,n_obs,seed,'cc',his_cc)
        all_meta = all_meta.append(meta_cc, ignore_index=True)

    all_meta.to_csv(f'{par}/meta.csv')        
    print(Counter(counter_list))