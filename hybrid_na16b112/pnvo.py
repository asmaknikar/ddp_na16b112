from sympy import symbols
import numpy as np
import pandas as pd
from scipy.spatial.transform import Rotation
from scipy import interpolate
from matplotlib import pyplot as plt
from tqdm.notebook import tqdm
import multiprocessing
import time
from multiprocessing import Pool

from common import (distance, get_azimuth_elevation,
                    get_uvec_from_azimuth_elevation, time_of_closest_approach,
                    unit_vector,plot_arrow,time_to_collision_ashish,time_to_collision_LPM)

def a_ppn(p_robot,v_robot,p_target,v_target,N,bounds):
    r = p_target - p_robot
    v_relative = v_target - v_robot
    # omega = np.cross([r[0],r[1],0],[v_relative[0],v_relative[1],0])/np.dot(r,r)
    omega = np.cross(r,v_relative)/np.dot(r,r)
    vr_cap = unit_vector(v_robot)
    # a_ppn = -N*np.cross([v_robot[0],v_robot[1],0],omega)
    a_ppn = -N*np.cross(v_robot,omega)
    # a_ppn = np.array([a_ppn[0],a_ppn[1]])
    if bounds==0:
        return a_ppn
    if bounds==1:
        return min(2,np.linalg.norm(a_ppn))*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)<2:
        a_ppn = 2*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)>3:
        a_ppn = 3*unit_vector(a_ppn)
    return a_ppn

def time_to_collision_VO(p_r,p_o,v_r,v_o,R):
    toc = abs((distance(p_o,p_r) - R)/np.dot(v_r-v_o,unit_vector(p_o-p_r)))
    return(toc)

def check_VO(p_r,v_r,p_t,v_t,alpha_vo=None,rho=None,R=3):
    """Check if in VO colllison course

    Parameters
    ----------
    p_r : ndarray(ndim=3)
        UAV position
    v_r : ndarray(ndim=3)
        UAV velocity
    p_t : ndarray(ndim=(3))
        Obstacles position
    v_t : ndarray(ndim=(3))
        Obstacles velocities
    alpha_vo : float, optional
        conic half angle, by default None
    rho : float, optional
        sensor range, by default None
    R : int, optional
        Obstacle radius, by default 3
    """
    if(alpha_vo is None):
#         R = 3
        d_oi = distance(p_r,p_t)
        d_vo = ((d_oi**2)-(R**2))/d_oi
        r_vo = R*((np.sqrt((d_oi**2)-(R**2)))/d_oi)
        alpha_vo = np.arctan(r_vo/d_vo)
    v_rel = v_r-v_t
    p_rel = p_t-p_r
    p_rel_mod = np.linalg.norm(p_rel)
    cond = True
    if(rho is not None):
        cond = p_rel_mod<rho
    return(((np.dot(v_rel,p_rel)/(np.linalg.norm(v_rel)*p_rel_mod))>np.cos(alpha_vo)) and cond)
  
def get_R_vo_thpsi(th,psi):
    """Get VO th psi rotation matrixto rotate cone axis from X-axis toward the UAV obstacle vecotr

    Parameters
    ----------
    th : float
        elevation
    psi : float
        azimuth
    
    Returns
    -------
    _ : ndarray(ndim=(3,3))
        Roation matrix
    
    Notes
    -----
    * Wrong in paper. I have repeatedly tried to verify paper matrix but was erroneus. 
    I have hence made and used my own which I have verified multiple times.
    """
    m1 = np.array(
        [[np.cos(psi),-np.sin(psi),0],
          [np.sin(psi),np.cos(psi),0],
          [0,0,1]])
    m2 = np.array(
        [[np.cos(th),0,-np.sin(th)],
          [0,1,0],
          [np.sin(th),0,np.cos(th)]])
#     print(m1,m2)
    return(np.matmul(m1,m2))

def get_R_Pphi(phi):
    """Generates Rotation matrix to rotate about x-axis by phi

    Parameters
    ----------
    phi : float
        angle of rotation
    
    Returns:
    _ : ndarray(ndim=(3,3))
        Rotation matrix
    """[description]
    return(np.array([[1,0,0],
                    [0,np.cos(phi),np.sin(phi)],
                    [0,-np.sin(phi),np.cos(phi)]]))

def get_vr2xaxis_rot(v_r):
    """Ger rotation matrix to move v_R along x-axis

    Parameters
    ----------
    v_r : ndarray(ndim=3)
        UAV velocity 
    
    Returns
    ------
    rot_vr : scipy.spatial.transform.Rotation
        Rotation matrix
    """
    v_r_new = [np.linalg.norm(v_r),0,0]
    rot_vr = Rotation.from_rotvec(np.arccos(np.dot(unit_vector(v_r_new),unit_vector(v_r)))*unit_vector(np.cross(v_r,v_r_new)))
    return(rot_vr)

def get_M_vo_phi(A,th_oi,psi_oi,alpha_vo,phi,num_linspace_b=1000):
    b = np.linspace(0,2*np.pi,num_linspace_b)

    num = (-(A[2]*np.cos(phi)) + A[1]*np.sin(phi))
    den = (np.cos(phi)*(np.sin(th_oi) + np.cos(th_oi)*np.sin(b)*np.tan(alpha_vo)) -
           np.sin(phi)*(np.cos(th_oi)*np.sin(psi_oi) +
                   (np.cos(b)*np.cos(psi_oi) - 
                    np.sin(b)*np.sin(th_oi)*np.sin(psi_oi)
                   )*np.tan(alpha_vo)))
    a = num/den

    M_cc_st = np.array([a,a*np.tan(alpha_vo)*np.cos(b),a*np.tan(alpha_vo)*np.sin(b)]).T
    M_cc = (np.matmul(get_R_vo_thpsi(th_oi,psi_oi),np.expand_dims(M_cc_st,2)))
    M_vo = (M_cc+np.expand_dims(A,-1))
    M_vo_phi = np.matmul(get_R_Pphi(phi),M_vo)
    M_vo_phi = np.squeeze(M_vo_phi)
    return(M_vo_phi)

def get_M_vo_phi(A,th_oi,psi_oi,alpha_vo,phi,num_linspace_b=1000):
    """Generate VO^phi matrix for UAV obstacle pair

    Parameters
    ----------
    A : ndarray(ndim=3)
        See paper
    th_oi : float
        Obs elevation wrt UAV
    psi_oi : float
        Obs aximuth wrt UAV
    alpha_vo : float
        Cone half angle
    phi : float
        See paper. Avoidance plane angle
    num_linspace_b : int, optional
        Number of etas to generate, by default 1000

    Returns
    ------
    M_vo_phi : ndarray(ndim=(num)linspace_b,3)
        VO^phi matrix. 
    
    Notes
    ------
    * After using the derived "a" value, the z value of the marix must be zero. 
    using paper "a" was giving eror. hence derived own
    """
    b = np.linspace(0,2*np.pi,num_linspace_b)

    num = (-(A[2]*np.cos(phi)) + A[1]*np.sin(phi))
    den = (np.cos(phi)*(np.sin(th_oi) + np.cos(th_oi)*np.sin(b)*np.tan(alpha_vo)) -
           np.sin(phi)*(np.cos(th_oi)*np.sin(psi_oi) +
                   (np.cos(b)*np.cos(psi_oi) - 
                    np.sin(b)*np.sin(th_oi)*np.sin(psi_oi)
                   )*np.tan(alpha_vo)))
    a = num/den

    M_cc_st = np.array([a,a*np.tan(alpha_vo)*np.cos(b),a*np.tan(alpha_vo)*np.sin(b)]).T
    M_cc = (np.matmul(get_R_vo_thpsi(th_oi,psi_oi),np.expand_dims(M_cc_st,2)))
    M_vo = (M_cc+np.expand_dims(A,-1))
    M_vo_phi = np.matmul(get_R_Pphi(phi),M_vo)
    M_vo_phi = np.squeeze(M_vo_phi)
    return(M_vo_phi)

def get_intersection(M_vo_phi,v_r,check_vo=True):
    """Gets intersection of conic section with circle of radis |v_r|

    Parameters
    ----------
    M_vo_phi : ndarray(ndim=(num_linpace_b,3))
        The VO cone matrix after rotting by R_phi
    v_r : ndarray(ndim=3)
        UAV velocity
    check_vo : bool, optional
        Redundant, by default True

    Notes
    -----
    We use univariate Spline to interpolate the discrete data and get a more accurate intersection
    """
    opp_eps = None
    arr = ((M_vo_phi[:,0]**2+M_vo_phi[:,1]**2)-(np.linalg.norm(v_r)**2))
    arr = np.clip(arr,-10,10)
    spl = interpolate.UnivariateSpline(np.arange(0,len(arr)),arr,s=0)
    roots = spl.roots()
    dec,nat = np.modf(roots)
    nat = nat.astype(int);dec = np.expand_dims(dec,-1)
    # print("natdec",nat,dec)
    pts = ((1-dec)*M_vo_phi[nat])+(dec*M_vo_phi[nat+1])
    
    eps_avo = np.mod(np.arctan2(pts[:,1],pts[:,0]),2*np.pi)
    if(not(len(eps_avo)==2 or len(eps_avo)==4)):
        print("EPSILON ERROR",eps_avo)
        raise Exception()
    eps_avo = np.sort(eps_avo)
    if(len(eps_avo)==4):
        opp_eps = np.array([eps_avo[1],eps_avo[2]])
    eps_avo = np.array([eps_avo[0],eps_avo[-1]])
    
    # if(check_vo):
    #     for i in np.argsort(np.mod(eps_avo,2*np.pi)):
    #         if(i%2==0):
    #             while(check_targ_vel_phi(eps_avo[i], phi, p_o_vo, p_r_vo, v_o_vo, alpha_vo)):
    #                 eps_avo[i]+=np.deg2rad(0.1)
    #         else:
    #             while(check_targ_vel_phi(eps_avo[i], phi, p_o_vo, p_r_vo, v_o_vo, alpha_vo)):
    #                 eps_avo[i]-=np.deg2rad(0.1)
    return(pts,eps_avo,opp_eps)

def check_targ_vel_phi(x,phi,p_o_vo,p_r_vo,v_o_vo,alpha_vo):
    v_rel = np.matmul(get_R_Pphi(phi).T,np.array([np.cos(x),np.sin(x),0]))-v_o_vo
    p_rel = p_o_vo-p_r_vo
    return(np.dot(unit_vector(v_rel),unit_vector(p_rel))>np.cos(alpha_vo))


def get_v_r_esc(p_r,v_r,p_os,v_os,R,rho=None,num_phis=12,single_phi=None,num_linspace_b=1000,max_obs=None,check_later=False,plot=None):
    """Generating the escape v_r

    Parameters
    ----------
    p_r : ndarray(ndim=3)
        UAV position
    v_r : ndarray(ndim=3)
        UAV velocity
    p_os : ndarray(ndim=(n_obs,3))
        Obstacles position
    v_os : ndarray(ndim=(n_obs,3))
        Obstacles velocities
    R : float
        Obstacles radius
    rho : float, optional
        sensor range, by default None
    num_phis : int, optional
        number of planes to generate, by default 12
    single_phi : float, optional
        the phi if only generating single plane, by default None
    num_linspace_b : int, optional
        umber of betas to generate, by default 1000
    max_obs : int, optional
        maximum obstacles to work on prioritised by time to collision, by default None
    check_later : bool, optional
        Flag weather to check for collision of generated escape velocity, by default False
    plot : bool, optional
        Flag to plot conic sections, by default None

    Returns
    -------
    all_phi_res : pd.Dataframe(columns=["phi", "v_r_esc", "eps_esc", "obs_ind","tocs","A_z"])
        A dataframe of the escape velocities for each phi

    """
    Rot = get_vr2xaxis_rot(v_r)
    p_r_vo = Rot.apply(p_r)
    v_r_vo = Rot.apply(v_r)
    p_os_vo = np.apply_along_axis(Rot.apply,1,p_os)#np.apply_along_axis(Rot.apply,1,p_os)
    v_os_vo = np.apply_along_axis(Rot.apply,1,v_os)#np.apply_along_axis(Rot.apply,1,v_os)
    
    checks = np.array([check_VO(p_r_vo,v_r_vo,p_oi,v_oi,rho=rho,R=R) for p_oi,v_oi in zip(p_os_vo,v_os_vo)])
    # tocs = np.array([time_of_closest_approach(p_r_vo,p_oi,v_r_vo,v_oi) for (p_oi,v_oi) in zip(p_os_vo[checks]
    #                                                                                           ,v_os_vo[checks])])
    # tocs = np.array([time_to_collision_LPM(p_r_vo,p_oi,v_r_vo,v_oi,R) for (p_oi,v_oi) in zip(p_os_vo[checks]
    #                                                                                           ,v_os_vo[checks])])
    tocs = np.array([time_to_collision_ashish(p_r_vo,p_oi,v_r_vo,v_oi,R) for (p_oi,v_oi) in zip(p_os_vo[checks]
                                                                                              ,v_os_vo[checks])])
          
    if(not(max_obs is None)):
        tocs[tocs<=0] = 256
        checks[np.nonzero(checks)[0][np.argsort(tocs)[max_obs:]]] = False#NP ARGS ERROR
        tocs = tocs[np.argsort(tocs)[:max_obs]]

    p_ocs_vo = p_os_vo[checks]
    v_ocs_vo = v_os_vo[checks]
    # print(p_ocs_vo.shape,v_ocs_vo.shape)

    d_ois = np.array([distance(p_r_vo,p_o_vo) for p_o_vo in p_ocs_vo])
    d_vos = ((d_ois**2)-(R**2))/d_ois
    r_vos = R*((np.sqrt((d_ois**2)-(R**2)))/d_ois)
    alpha_vos = np.arctan(r_vos/d_vos)
    psi_ois,th_ois = np.array([get_azimuth_elevation(p_o_vo-p_r_vo) for p_o_vo in p_ocs_vo]).T
    
    As = v_ocs_vo
    Ds = np.expand_dims(d_vos,-1)*(np.array([unit_vector(p_o_vo-p_r_vo) for p_o_vo in p_ocs_vo]))

    phis = np.linspace(0,np.pi,num_phis)
    if(single_phi is not None):
        phis = np.array([single_phi])
    all_phi_res = list()
    if(plot):
        fig, axs = plt.subplots(2,int(np.ceil(len(phis)/2)), figsize=(15, 6), facecolor='w', edgecolor='k')
        fig.subplots_adjust(hspace = .5, wspace=.001)
        axs = axs.ravel()
    if(plot is None or plot==False):
        axs = np.zeros_like(phis)
    
    for ax,phi in zip(axs[:],phis[:]):
        try:
            # tb0 = time.time()
            # del_p_phi = np.arccos(np.dot(unit_vector(D),[0,-np.sin(phi),np.cos(phi)]))
            Az = np.sum((np.apply_along_axis(lambda x:np.matmul(get_R_Pphi(phi),x),1,As)*
                         np.expand_dims(unit_vector(1/tocs),-1))[...,2])
            M_vo_phis = np.array([get_M_vo_phi(A,th_oi,psi_oi,alpha_vo,phi,num_linspace_b=1000) 
                                  for A,th_oi,psi_oi,alpha_vo in zip(As,th_ois,psi_ois,alpha_vos)])


            pts_all,eps_avo_all,opp_eps_all = list(),list(),list()
            for M_vo_phi in M_vo_phis:
                pts,eps_avo,opp_eps = get_intersection(M_vo_phi,v_r,check_vo=False)
                pts_all.append(pts)
                eps_avo_all.append(eps_avo)
                if(opp_eps is not None):
                    opp_eps_all.append(opp_eps)
            # pts_all = np.array(pts_all)
            eps_avo_all = np.array(eps_avo_all)
            opp_eps_all = np.array(opp_eps_all)
            # print("pts,evo,opp_eps all", pts_all.shape,eps_avo_all.shape,opp_eps_all)

            # print("eps",np.rad2deg(eps_avo))
            # print("check_eps",[[check_targ_vel_phi(x, phi, p_o_vo, p_r_vo, v_o_vo, alpha_vo) for x in eps_avo]
            #                    for eps_avo in eps_avo_all])


            # print("cehck_v",[[check_VO(p_r_vo,
            #      np.matmul(get_R_Pphi(phi).T,np.append(v_avoi,0)),
            #      p_o_vo,v_o_vo,alpha_vo,R=R) for v_avoi in v_avos] for v_avos in v_avos_all])

            top_eps,bot_eps = np.max(eps_avo_all,0)[0],np.min(eps_avo_all,0)[1]
            top_eps_arg,bot_eps_arg = np.argmax(eps_avo_all,0)[0],np.argmin(eps_avo_all,0)[1]
            if(top_eps>bot_eps):
                print("ESCAPE NOT POSSIBLE FOR THIS V_R")
            eps_esc,arg_esc,top_or_bot = (top_eps,top_eps_arg,1) if top_eps<((2*np.pi)-bot_eps) else (bot_eps,bot_eps_arg,-1)
            v_r_vo_phi_esc = np.linalg.norm(v_r_vo)*np.array([np.cos(eps_esc),np.sin(eps_esc),0])
            v_r_vo_esc = np.matmul(get_R_Pphi(phi).T,v_r_vo_phi_esc)
            v_r_esc = Rot.inv().apply(v_r_vo_esc)
            v_r_esc = Rotation.from_rotvec(np.deg2rad(2)*unit_vector(np.cross(v_r,v_r_esc))).apply(v_r_esc)
            if(plot):
                plt.axis("equal")
                ax.plot(np.linalg.norm(v_r)*np.cos(np.linspace(0,2*np.pi,20)),
                         np.linalg.norm(v_r)*np.sin(np.linspace(0,2*np.pi,20)))#plotting circle
                [ax.plot(M_vo_phi[:,0],M_vo_phi[:,1],color='grey') for M_vo_phi in M_vo_phis]#plotting conic sections
                ax.set_xlim(*(2*np.array([-np.linalg.norm(v_r),np.linalg.norm(v_r)])))
                ax.set_ylim(*(2*np.array([-np.linalg.norm(v_r),np.linalg.norm(v_r)])))
                ax.set_title(f"{np.rad2deg(phi):.2f},{Az:.2f}")
                ax.set_aspect(1)
                
                ax.arrow(0,0,np.linalg.norm(v_r_vo),0)
                v_avos_all = [np.linalg.norm(v_r_vo)*np.array([np.cos(eps_avo),np.sin(eps_avo)]).T 
                              for eps_avo in eps_avo_all]
                for v_avos in v_avos_all:
                    for v_avo in v_avos:
                        ax.arrow(0,0,v_avo[0],v_avo[1])
                ax.arrow(0,0,v_r_vo_phi_esc[0],v_r_vo_phi_esc[1],color='green')

            i = 0
            if(check_later):
                while(len(np.nonzero([check_VO(p_r,v_r_esc,p_oi,v_oi,rho=rho,R=R) for p_oi,v_oi in zip(p_os,v_os)])[0])>0):
                    i+=3
                    v_r_esc = Rotation.from_rotvec(np.deg2rad(1)*unit_vector(np.cross(v_r,v_r_esc))).apply(v_r_esc)
                    if(i>25):
                        print("reverting to orignal v_r_esc",end = " ")
                        raise Exception()
                        v_r_esc = Rot.inv().apply(v_r_vo_esc)
                        break
            # print("V_R esc",v_r_esc,top_or_bot,arg_esc,np.nonzero(checks)[0][arg_esc])
            all_phi_res.append([phi,
                                v_r_esc,
                                eps_esc+(np.deg2rad(1)*i),
                                np.nonzero(checks)[0][arg_esc],
                                tocs[arg_esc],
                                Az])
        except:
            print("skipped",phi)
            continue

    if(len(all_phi_res)>0):
        # all_phi_res = np.array(all_phi_res)
        all_phi_res = pd.DataFrame(all_phi_res, columns=["phi", "v_r_esc", "eps_esc", "obs_ind","tocs","A_z"])

        del_angs = np.array([np.arccos(np.dot(unit_vector(arr),unit_vector(v_r))) for arr in np.array(all_phi_res.v_r_esc.values)])
        obs_inds = all_phi_res.obs_ind.values.astype(int)
        # tocs = np.array([time_to_collision_VO(p_r,p_os[i],v_r,v_os[i],R) for i in obs_inds])
        omega_avos = del_angs/all_phi_res.tocs.values
        all_phi_res["omega_avos"] = omega_avos
        # all_phi_res['tocs'] = tocs
        all_phi_res['angs'] = del_angs
    else:
        all_phi_res = None

    # ta1 = time.time();print("tot time",ta1-ta0 )
    return(all_phi_res)



def pnvo(p_r,v_r,p_t,v_t,p_os,v_os,N=3,K=3,R=15,rho=None,num_phis=12,num_linspace_b=1000,max_obs=None,check_later=False,plot=False,plot_debug=False):
    """ Module for simulation PN-VO(Velocity Obstacle module)

    Parameters
    ----------
    p_r : ndarray(ndim=3)
        Initial UAV position
    v_r : ndarray(ndim=3)
        Initial UAV velocity
    p_t : ndarray(ndim=3)
        Initial target position
    v_t : ndarray(ndim=3)
        Initial target velocity
    p_os : ndarray(ndim=(n_obs,3))
        Initial obstacles positions
    v_os : ndarray(ndim=(n_obs,3))
        Initial obstacles velocities        
    N : float, optional
        PPN constant, by default 3
    K : float, optional
        VO proportionality constant, by default 3
    R : float, optional
        Obstacle radius, by default 15
    rho : float, optional
        sensor range, by default None
    num_phis : int, optional
        number of avoidance planes to generate solutions for at angle phi, by default 12
    num_linspace_b : int, optional
        numberof beta parameters in linspace(0,2 pi), by default 1000
    max_obs : int, optional
        max number of obstacle to tackle at a time prioritised by time to collision, by default None
    check_later : bool, optional
        Flag  to check for collision of final derived escape velocity(can cincreasae computation time), by default False
    plot : bool, optional
        Flag for plotting, by default False
    plot_debug : bool, optional
        Flag for plotting the conic sections as well, by default False

    Refereneces
    ------------
    Yazdi I. Jenie et al. “Three-Dimensional Velocity Obstacle Method for Uncoordi-nated Avoidance Maneuvers
     of Unmanned Aerial Vehicles”.In:Journal of Guid-ance, Control, and Dynamics39 (10 Oct. 2016).
     ISSN: 0731-5090.DOI:10.2514/1.G001715
    
    Notes:
    ------
    * Do not use too low a value of num_linspace_b, results in imprecise intersetion and velocities
    * check_later is created to prevent any errors arising from rotating the escape velocity back to original frame. Can slow down program
    * Optimise num_linspace_b,num_phis and check_later(even putting false gives good results) for speed
    * The rotation matrices and derivation in paper are erroneous. I have verified my derivation in jupyter and mathematica

    """
    dt = 0.05
    his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_vo","a_ppn","accel","closest_dis","obs","comp_time","collision"])

    if(plot):
        fig = plt.figure()
        ax=fig.add_subplot(111,projection='3d')
        plot_arrow(ax,p_r,v_r,"o","blue")
        plot_arrow(ax,p_t,v_t,"o","green")
        plot_arrow(ax,p_os,v_os,"D","red")
        plt.show()

    time_cc = 0
    a_vo_prev = np.array([0,0,0])
    omega_prev = 0
    omega = 0
    time_cc_at_vo_calc = 0
    S_coll_prev = set()
    # count_avoidance = 0

    omega_vo_bound = np.deg2rad(30)
    accel_vo_bound = np.linalg.norm(v_r)*omega_vo_bound
    print("accel_bound",accel_vo_bound)

    if(rho is None):
        rho = 1*np.sqrt((2*((12.5+10)/omega_vo_bound)+R)*R)
    mindist = rho

    sim_time_st = time.time()
    while distance(p_r,p_t)>2:
        collision = False
        comp_time_st = time.time()
        S_coll = np.nonzero(np.array([check_VO(p_r,v_r,p_oi,v_oi,rho=rho,R=R) for p_oi,v_oi in zip(p_os,v_os)]))[0]
        min_dist = np.min(np.linalg.norm(p_os-p_r,axis=1))
        if(min_dist<R):
            collision = True
            print("collision")
        a_vo = np.array([0,0,0])
        accel_ppn = a_ppn(p_r,v_r,p_t,v_t,N,0)
        if(len(S_coll)>0):
            # count_avoidance+=1
            try:
                if(set(S_coll).issubset(S_coll_prev) and (time_cc-time_cc_at_vo_calc)<0.1 and min_dist>R+0.8):
                    print("using_prev",end= " ")
                    a_vo = np.cross(unit_vector(np.cross(v_r,tmp.v_r_esc))*omega_prev*K,v_r)
                else:
                    time_cc_at_vo_calc = time_cc
                    all_phi_res = get_v_r_esc(p_r,v_r,p_os,v_os,R,rho=rho,num_phis=num_phis,num_linspace_b=num_linspace_b,max_obs=max_obs,check_later=check_later)
                    if(all_phi_res is not None):
                        ind = np.argmax(unit_vector(all_phi_res.omega_avos)*(-0.7)+
                                                        unit_vector(np.abs(all_phi_res.A_z))*0.3)
                        tmp = all_phi_res.iloc[ind]
                        omega = tmp.omega_avos
                        if(np.abs(tmp.angs)>np.deg2rad(7)):
                            a_vo = np.cross(unit_vector(np.cross(v_r,tmp.v_r_esc))*omega*K,v_r)
                        else:
                            a_vo = np.cross(unit_vector(np.cross(v_r,tmp.v_r_esc))*omega_prev*K,v_r)
                    elif(set(S_coll)==S_coll_prev):
                        a_vo = np.cross(unit_vector(np.cross(v_r,tmp.v_r_esc))*omega_prev*K,v_r)
            except:
                all_phi_res = get_v_r_esc(p_r,v_r,p_os,v_os,R,rho=rho,num_phis=12,plot=plot_debug)
            a_vo = a_vo+np.dot(accel_ppn,unit_vector(a_vo))*(-unit_vector(a_vo))    #Removing PPN comp from A_VO
        
        accel = a_vo+accel_ppn
        accel = min(np.linalg.norm(accel),accel_vo_bound)*unit_vector(accel)
        comp_time_end = time.time()
        
        print(distance(p_r,p_t),round(np.linalg.norm(accel),2),round(np.linalg.norm(a_vo),2),S_coll,min_dist)
        if(distance(p_r,p_t)>300):
            his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_cc","a_ppn","accel","closest_dis","obs","comp_time","collision"])
            break
        v_r = v_r + (accel*dt)
        p_r = p_r + v_r*dt
        time_cc = time_cc+dt
        p_os = p_os+(v_os*dt)
        p_t = p_t + (v_t*dt)

        # S_coll_prev = set(S_coll)
        S_coll_prev = S_coll_prev if(len(S_coll)==0) else set(S_coll)
        a_vo_prev = np.copy(a_vo)
        omega_prev = np.copy(omega)

        his = his.append(pd.Series([p_r,v_r,p_t,v_t,p_os,v_os,
                                    a_vo,accel_ppn,accel,
                                    min_dist,S_coll,comp_time_end-comp_time_st,collision], index = his.columns),ignore_index=True)
    metadata = dict(R=R,rho=rho,
                    tot_sim_comp_time = time.time()-sim_time_st,
                    trunc_sim_comp_time = np.sum(his.comp_time.values),
                    obs_collisions = len(np.nonzero(his.collision.values)[0]),
                    obs_avo_steps=len(np.nonzero(np.array(list(map(len,his.obs.values)))>0)[0]),
                    a_net_rms = np.sqrt(np.mean(np.apply_along_axis(lambda x:np.dot(x,x),1,np.stack(his.accel.values)))) if (len(his)) else 0,
                    len = len(his))
    return(his,metadata)
            
