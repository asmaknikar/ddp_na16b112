import matplotlib.animation as animation
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import numpy as np
from scipy.spatial.transform import Rotation
from tqdm import tqdm
from pathlib import Path


def generate_workspace(seed,start,end,n_obs,v_r_mod=12.5,v_t_mod=7.5,v_obs_range=(5,10),r_rand_ang_lim=(0,45),t_rand_ang_lim=(0,45),R=0):
    """Generates workspace for simulation

    Parameters
    ----------
    seed : int
        The input seed for numpy random functions
    start : int
        (start,start,start) is the beginning of the workspace
    end : [type]
        (end,end,end) is the end of the workspace
    n_obs : [type]
        [description]
    v_r_mod : float, optional
        |v_r|, by default 12.5
    v_t_mod : float, optional
        |v_t|, by default 7.5
    v_obs_range : tuple, optional
        (min(|v_obs|),max(|v_obs|)), by default (5,10)
    r_rand_ang_lim : tuple, optional
        The angle limit for random rotation of v_rvector
    t_rand_ang_lim : tuple, optional
        The angle limit for random rotation of v_t vector, by default 45
    R : int, optional
        The radius of obstacle. We use it to generate obstacles at a distance of 2*(R,R,R) from p_r, by default 0

    Notes
    ------
    * We intialise the v_r and v_t vecs towards p_t and p_r respectively. We then rotate them about a random axis by a random angle(b/n -+v_rt_rand_an_lim).
        This is to ensure that the robot and targets pass throught he uniform obstacle cloud generated in the workspace
    """ 

    np.random.seed(seed)
    p_r = np.array([0,0,0])
    p_t = np.array([end,end,end])
    v_r = v_r_mod*Rotation.from_rotvec(
        np.random.uniform(*np.deg2rad(r_rand_ang_lim))*unit_vector(np.random.rand(3))).apply(unit_vector(p_t-p_r))
    v_t = v_t_mod*Rotation.from_rotvec(
        np.random.uniform(*np.deg2rad(t_rand_ang_lim))*unit_vector(np.random.rand(3))).apply(unit_vector(p_r-p_t))

    p_os,v_os = (np.array([np.random.uniform(start+1*R,end,n_obs),
                        np.random.uniform(start+1*R,end,n_obs),
                        np.random.uniform(start+1*R,end,n_obs)]).T,
                np.array([np.random.uniform(*v_obs_range)*v for v in np.apply_along_axis(unit_vector,1,np.random.uniform(-1,1,(n_obs,3)))]))
    
    return(p_r,v_r,p_t,v_t,p_os,v_os)

def save_csv(par,n_obs,seed,algo,his):
    out_file = Path(f'{par}/{n_obs}/{seed}/{algo}.csv')
    out_file.parent.mkdir(parents=True, exist_ok=True)
    his.to_csv(out_file)

def unit_vector(vector):
    """Calculating the unit vector of a vector

    Args:
        vector (ndarray): input vector

    Returns:
        ndarray: unit vector
    """
    if np.linalg.norm(vector) == 0:
        return 0
    return vector/np.linalg.norm(vector)

def distance(a,b):
    """Distance between two vectors

    Args:
        a (ndarray): vector 1
        b (ndarray): vector 2
    """
    return(np.linalg.norm(a-b))
#     return math.sqrt(np.dot(a-b,a-b))


def angle_between(v1, v2):
    return np.math.atan2(np.linalg.det([v1,v2]),np.dot(v1,v2))


def get_azimuth_elevation(v):
    return(np.arctan2(v[1],v[0]),np.arctan2(v[2],np.linalg.norm([v[0],v[1]])))#azimuth, elevation

def get_uvec_from_azimuth_elevation(a,e):
    return(np.array([np.cos(e)*np.cos(a),np.cos(e)*np.sin(a),np.sin(e)]))


def time_of_closest_approach(p_r,p_t,v_r,v_t):
    """The time of closes approach to an obstacle
    """
    p_rel = p_t-p_r
    v_rel = v_r-v_t #The velocity is taken as v_t-v_r in Collision cone paper. We compenasate by removing the minus sign while calculating time
    v_radial = np.dot(v_rel,unit_vector(p_rel))
    toc = (np.linalg.norm(p_rel)*v_radial)/(np.dot(v_rel,v_rel))
    return(toc)

def time_to_collision_ashish(p_r,p_o,v_r,v_o,R=0):
    """
    Returns the exact time to collide with circlular obstacle of rad R without external accn.
    First calculates distanc eof point from circle along v_r and then time taken to traverse it.
    The calculation of distance of point from surface of sphere can be solved with law of cosines
    https://en.wikipedia.org/wiki/Law_of_cosines#Applications
    The third formula a = ...;b=|p_o-p_r|,c=R.
    We take the minus sign since by SSA congruency the second triangle will be formed with side
    going through circle
    Hence the shorter on will intersect the circle and not pass through.(the one we want)
    """
    p_rel = p_o-p_r
    v_rel = v_r-v_o
    doi = np.linalg.norm(p_rel)
    ang = np.arccos(np.dot(unit_vector(v_rel),unit_vector(p_rel)))
    if((R==0) or (ang>np.pi/2) or (doi*np.sin(ang)>=R) or (doi<R)):
        return(time_of_closest_approach(p_r,p_o,v_r,v_o))
    else:
        dis = doi*np.cos(ang)-np.sqrt((R**2)-((doi*np.sin(ang))**2))
        return(dis/np.linalg.norm(v_rel))

def time_to_collision_LPM(p_r,p_o,v_r,v_o,R=0):
    toc = abs((distance(p_o,p_r) - R)/np.dot(v_r-v_o,unit_vector(p_o-p_r)))
    return(toc)



def plot_arrow(ax,p,v,marker="o",color='blue',qcolor='blue'):
    """Plot arrow in given Axes object
    """
    ax.scatter3D(p[...,0],p[...,1],p[...,2],s=10,marker=marker,color=color)
    ax.quiver(p[...,0],p[...,1],p[...,2],v[...,0],v[...,1],v[...,2],color=color)

def plot_cone(ax,d_vo,alpha_vo,Rot_mat,offset=[0,0,0]):
    # Set up the grid in polar
    beta = np.linspace(0,2*np.pi,30)
    a = np.linspace(0,d_vo,50)
    B, A = np.meshgrid(beta, a)

    # Then calculate X, Y, and Z
    # Z = R * np.cos(T)
    # Y = R * np.sin(T)h
    # X = np.sqrt(Z**2 + Y**2) - 1
    X = A
    Y = A * np.tan(alpha_vo) * np.cos(B)
    Z = A * np.tan(alpha_vo) * np.sin(B)
#     func = lambda l : np.matmul(get_R_vo_thpsi(th_oi,psi_oi),l)
    func = lambda l : np.matmul(Rot_mat,l)
    
    X,Y,Z = np.apply_along_axis(func,0,np.array([X,Y,Z]))

    thp,php = np.linspace(0,2*np.pi,30),np.linspace(0,2*np.pi,30)
    Thp,Php = np.meshgrid(thp,php)

    ax.plot_wireframe(X+offset[0],
                      Y+offset[1],
                      Z+offset[2])
    
    
def plot_sphere(ax,R,offset,color='yellow'):
    thp,php = np.linspace(0,2*np.pi,30),np.linspace(0,2*np.pi,30)
    Thp,Php = np.meshgrid(thp,php)
    ax.plot_wireframe(offset[0]+R*np.sin(Php)*np.cos(Thp),
                      offset[1]+R*np.sin(Php)*np.sin(Thp),
                      offset[2]+R*np.cos(Php),color=color)

    
def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def plot_gif(his):
    dt = 0.01
    pbar = tqdm()
    req_fps = 10

    def animate(frame,h,h2,his,pbar):
    #     print(frame)
    #     h.set_xdata(p_curr[...,0])
    #     h.set_ydata(p_curr[...,1])
    #     print(frame)
        tmp = np.stack(his.p_r)
        h[0].set_data(tmp[frame,0],tmp[frame,1])
        h[0].set_3d_properties(tmp[frame,2])
        
        tmp = np.stack(his.p_t)
        h[1].set_data(tmp[frame,0],tmp[frame,1])
        h[1].set_3d_properties(tmp[frame,2])
        
        tmp = np.stack(his.p_os)
        for i in range(tmp.shape[1]):
            h2[i].set_data(tmp[frame,i,0],tmp[frame,i,1])
            h2[i].set_3d_properties(tmp[frame,i,2])
        pbar.update(frame-pbar.n)
        pbar.refresh()
    #     return(h[0],h[1],h2)
        
    def gen(ma,dt):
        i = 0
        req_fps = 30
        curr_fps = 1/dt
        incr = int(max(1,curr_fps//req_fps))
        yield(i)
        while(i+3<ma):
            i+=incr
            
            yield(i)
        if(i<ma):
            yield(ma-1)
        
    fig = plt.figure()
    ax = p3.Axes3D(fig)

    tmp = np.stack(his.p_r)
    ax.plot(tmp[:,0],tmp[:,1],tmp[:,2],color='blue')
    h = [ax.plot(tmp[0:1,0],tmp[0:1,1],tmp[0:1,2],marker="D",color='blue',linestyle='None')[0]]
    tmp = np.stack(his.p_t)
    ax.plot(tmp[:,0],tmp[:,1],tmp[:,2],color='green')
    h.append(ax.plot(tmp[0:1,0],tmp[0:1,1],tmp[0:1,2],marker="D",color='green',linestyle='None')[0])
    tmp = np.stack(his.p_os)
    h2 = [ax.plot(tmp[0:1,i,0],tmp[0:1,i,1],tmp[0:1,i,2],marker=".",color='red',linestyle='None')[0] for i in range(tmp.shape[1])]

    pbar.reset(total=his.shape[0])

    anim = animation.FuncAnimation(fig, animate,range(0,his.shape[0],int(max(1,(1/dt)//req_fps))) ,
                                fargs=(h,h2,his,pbar),interval=(1000//req_fps))
    plt.show()
    pbar.close()
    print(anim.save('PF.gif', writer=animation.PillowWriter(req_fps)))
