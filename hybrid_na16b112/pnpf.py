import copy
import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits import mplot3d

from common import (angle_between, distance, plot_arrow,
                    time_of_closest_approach, unit_vector,time_to_collision_ashish,time_to_collision_LPM,generate_workspace,save_csv)


def a_ppn(p_robot,v_robot,p_target,v_target,N,bounds):
    r = p_target - p_robot
    v_relative = v_target - v_robot
    # omega = np.cross([r[0],r[1],0],[v_relative[0],v_relative[1],0])/np.dot(r,r)
    omega = np.cross(r,v_relative)/np.dot(r,r)
    vr_cap = unit_vector(v_robot)
    # a_ppn = -N*np.cross([v_robot[0],v_robot[1],0],omega)
    a_ppn = -N*np.cross(v_robot,omega)
    # a_ppn = np.array([a_ppn[0],a_ppn[1]])
    if bounds==0:
        return a_ppn
    if bounds==1:
        return min(2,np.linalg.norm(a_ppn))*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)<2:
        a_ppn = 2*unit_vector(a_ppn)
    if np.linalg.norm(a_ppn)>3:
        a_ppn = 3*unit_vector(a_ppn)
    return a_ppn



def perpendicular_to(v):
    v_homogenous = np.array([v[0],v[1],1])
    direction = np.cross(np.array([0,0,1]),v_homogenous)
    return np.array([direction[0],direction[1]])

def time_to_col(p1,p2,v1,v2,rad):#CHECK DOT PRODUCT SHUD BE v1-v2.p2-p1
    toc = abs((distance(p1,p2) - 2*rad)/np.dot(v1-v2,unit_vector(p1-p2)))
    return toc

def time_to_col2(p_r,p_t,v_r,v_t,rad=0):
    toc = ((distance(p_r,p_t) - rad)/np.dot(v_r-v_t,unit_vector(p_t-p_r)))
    return toc
    

def get_azimuth_elevation(v):
    return(np.arctan2(v[1],v[0]),np.arctan2(v[2],np.linalg.norm([v[0],v[1]])))#azimuth, elevation

def get_uvec_from_azimuth_elevation(a,e):
    return(np.array([np.cos(e)*np.cos(a),np.cos(e)*np.sin(a),np.sin(e)]))



def pnpf(p_r,v_r,p_t,v_t,p_os,v_os,N=3,k_rep=120,k_att=0,R=3,rho=None,plot=False):
    """Simulate the PN-PF algorithm(Proportional Navigation-Potential Field)

    Parameters
    ----------
    p_r : ndarray(ndim=3)
        Initial robot position
    v_r : ndarray(ndim=3)
        Initial UAV velocity
    p_t : ndarray(ndim=3)
        Initial target position
    v_t : ndarray(ndim=3)
        Initial target velocity
    p_os : ndarray(ndim=(n_obs,3))
        Initial obstacle positions
    v_os : ndarray(ndim=(n_obs,3))
        Initial obstacles velocities
    N : int, optional
        PPN constant, by default 3
    k_rep : int, optional
        PF repulsion parameter, by default 120
    k_att : int, optional
        PF attravtion param, by default 0
    R : int, optional
        The obstacels radius, by default 3
    rho : [type], optional
        The maximum distance that the UAV can sense obstacles, by default None
    plot : bool, optional
        Flag to generate plots, by default False

    Returns
    ----------
    his : Pandas Dataframe
        The pandas dataframe comprising position and acceleration history, simulation data
    
    Notes
    ---------
    Ashish
    """
    his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_pf","a_ppn","accel","closest_dis","obs","comp_time","collision"])

    if(plot):
        fig = plt.figure()
        ax=fig.add_subplot(111,projection='3d')
        plot_arrow(ax,p_r,v_r,"o","blue")
        plot_arrow(ax,p_t,v_t,"o","green")
        plot_arrow(ax,p_os,v_os,"D","red")

        plt.show()
    # time.sleep(3)

    dt = 0.05

    least_distance = R

    time_cc = 0
    omega_bound = np.deg2rad(30)
    accel_bound = np.linalg.norm(v_r)*omega_bound
    print("accel_bound",accel_bound)
    max_obs = 10
    sim_time_st = time.time()


    if(rho is None):
        rho = 1*np.sqrt((2*((12.5+10)/omega_bound)+R)*R)
    print("rho=",rho)
    sim_time_st = time.time()
    # S_coll_prev = set()
    while distance(p_r,p_t)>2:
        collision = False
        comp_time_st = time.time()
        dist_os = np.linalg.norm(p_os-p_r,axis=1)

        if(np.min(dist_os)<least_distance):
            print("collision")
            collision = True

        ind_rhos = np.nonzero(dist_os<rho)[0]
        # tocs_rhos = np.array([time_of_closest_approach(p_r,p_oi,v_r,v_oi) for p_oi,v_oi in zip(p_os[ind_rhos],v_os[ind_rhos])])
        tocs_rhos = np.array([time_to_collision_ashish(p_r,p_oi,v_r,v_oi) for p_oi,v_oi in zip(p_os[ind_rhos],v_os[ind_rhos])])
        
        #removing negative tocs
        ind_rhos = ind_rhos[tocs_rhos>0]
        tocs_rhos = tocs_rhos[tocs_rhos>0]

        if(len(ind_rhos)>0):
            #selecting top n obstacles
    #         #by toc
    #         ind_rhos = ind_rhos[np.argsort(tocs_rhos)[:max_obs]]
    #         tocs_rhos = tocs_rhos[np.argsort(tocs_rhos)[:max_obs]]
            
            #by dist
    #         ind_rhos = ind_rhos[np.argsort(dist_os[ind_rhos])[:max_obs]]
    #         tocs_rhos = tocs_rhos[np.argsort(dist_os[ind_rhos])[:max_obs]]
            
            #by weight of both
            weighted_vals = 0.9*unit_vector(dist_os[ind_rhos])+0.1*unit_vector(tocs_rhos)
            ind_rhos = ind_rhos[np.argsort(weighted_vals)[:max_obs]]
            tocs_rhos = tocs_rhos[np.argsort(weighted_vals)[:max_obs]]
            
            p_rels = p_os[ind_rhos]-p_r
            p_rels = np.expand_dims((np.linalg.norm(p_rels,axis=1)-
                                    np.where(dist_os[ind_rhos]>least_distance,least_distance,0)),-1)*np.apply_along_axis(unit_vector,1,p_rels)
            a_reps = k_rep*(-p_rels/np.expand_dims(np.linalg.norm(p_rels,axis=1)**3,-1))
    #         a_reps = np.expand_dims((1/np.linalg.norm(p_rels,axis=1))-(1/rho),-1)*a_reps#multiplying the 11/||-/rho part
    #         a_rep = np.sum(a_reps,axis=0)
            # a_rep = np.sum(np.expand_dims(unit_vector(1/np.abs(tocs_rhos)),-1)*a_reps,0)#reciprocal
            # a_rep = np.sum(np.expand_dims(unit_vector(1/np.abs(weighted_vals)),-1)*a_reps,0)#reciprocal
            #if no weights
            weighted_vals = np.ones_like(weighted_vals)
            a_rep = np.sum(np.expand_dims(np.abs(weighted_vals),-1)*a_reps,0)#reciprocal


            
            #IF YOU WANT ONLY TURNING
            ##closest to arep and perp to v_r((vrXa_rep)Xv_r)
            a_rep = np.linalg.norm(a_rep)*unit_vector(np.cross(np.cross(v_r,a_rep),v_r))
        else:
            a_rep = np.array([0,0,0])
        

        a_att = k_att*(p_t-p_r)
        ppn_accel = a_ppn(p_r,v_r,p_t,v_t,N,0)
        #neutralising ppn from rep
        a_rep = a_rep+np.dot(ppn_accel,unit_vector(a_rep))*(-unit_vector(a_rep))
        #bounding a_rep
        # a_rep = min(np.linalg.norm(a_rep),accel_bound)*unit_vector(a_rep)

        accel = a_rep+a_att+ppn_accel
        accel = min(np.linalg.norm(accel),accel_bound)*unit_vector(accel)
        comp_time_end = time.time()
        
        print(dict(targ_dist=distance(p_r,p_t),
                    a_net=round(np.linalg.norm(accel),2),
                    a_rep = np.linalg.norm(a_rep),
                    v_r = np.linalg.norm(v_r),
                    min_ob_dis = np.min(dist_os),
                    obs = ind_rhos))
        if(distance(p_r,p_t)>300):
            his = pd.DataFrame(columns=["p_r","v_r","p_t","v_t","p_os","v_os","a_cc","a_ppn","accel","closest_dis","obs","comp_time","collision"])
            break
        v_r = v_r + (accel*dt)
        p_r = p_r + v_r*dt
        time_cc = time_cc+dt
        p_os = p_os+(v_os*dt)
        p_t = p_t + (v_t*dt)
        his = his.append(pd.Series([p_r,v_r,p_t,v_t,p_os,v_os,
                                    a_att+a_rep,ppn_accel,accel,
                                    np.min(dist_os),ind_rhos,comp_time_end-comp_time_st,collision], index = his.columns),ignore_index=True)
    metadata = dict(R=R,rho=rho,
                    tot_sim_comp_time = time.time()-sim_time_st,
                    trunc_sim_comp_time = np.sum(his.comp_time.values),
                    obs_collisions = len(np.nonzero(his.collision.values)[0]),
                    obs_avo_steps=len(np.nonzero(np.array(list(map(len,his.obs.values)))>0)[0]),
                    a_net_rms = np.sqrt(np.mean(np.apply_along_axis(lambda x:np.dot(x,x),1,np.stack(his.accel.values)))) if (len(his)) else 0,
                    len = len(his))
    return(his,metadata)
            
if __name__=="__main__":
    from collections import Counter
    from datetime import datetime
    # seeds = np.load("./seeds_R_4_rho_20_only_coll_for_pf.npy")
    seeds = np.load("./seeds_R_4_rho_20_for_pf.npy")
    # print(seeds)
    timestamp = datetime.now().strftime('%d_%m_%Y_%H_%M_%S')
    par = f"{timestamp}_toc_ash_pnpf_k_300_no_weights_with_margin_ofsafety1"
    

    plot = False
    R = 4
    rho = 20
    counter_list = []
    all_meta = pd.DataFrame()

    for seed,n_obs in seeds:
        p_r,v_r,p_t,v_t,p_os,v_os = generate_workspace(seed,0,100,n_obs,t_rand_ang_lim=(0,10),R=R)

        print("\n\n\n\nPNPF \n")
        print(seed,n_obs)

        his_pf,meta_pf = pnpf(p_r,v_r,p_t,v_t,p_os,v_os,N=3,k_rep=250,k_att=0,R=R,rho=rho,plot=plot)
        meta_pf['algo']='pf';meta_pf["n_obs"] = n_obs;meta_pf["seed"] = seed

        print(meta_pf["obs_collisions"])
        if(meta_pf["obs_collisions"]>0):
            counter_list.append(n_obs)
        print("COUNTER",Counter(counter_list))
        if(meta_pf["obs_avo_steps"]<10):
            print("SHIZNIT")

        save_csv(par,n_obs,seed,'pf',his_pf)
        all_meta = all_meta.append(meta_pf, ignore_index=True)

    all_meta.to_csv(f'{par}/meta.csv')        
    print(Counter(counter_list))